#!/bin/bash

# Avoid PGO profiling problems due to enviroment leakage
# These should *always* be cleaned up anyway
unset \
	DBUS_SESSION_BUS_ADDRESS \
	DISPLAY \
	ORBIT_SOCKETDIR \
	SESSION_MANAGER \
	XAUTHORITY \
	XDG_CACHE_HOME \
	XDG_SESSION_COOKIE

