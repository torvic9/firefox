# Maintainer: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Jakub Schmidtke <sjakub@gmail.com>

pkgname=firefox
pkgver=128.8.0esr
pkgrel=1
pkgdesc="Standalone web browser from mozilla.org"
arch=(x86_64)
license=(MPL GPL LGPL)
url="https://www.mozilla.org/firefox/"

depends=(alsa-lib at-spi2-core cairo dbus ffmpeg fontconfig gdk-pixbuf2 glib2 glibc
	hicolor-icon-theme gtk3 libpulse libx11 libxcb libxcomposite libxdamage libxext
	libxfixes libxrandr libxss libxt mime-types nss ttf-font icu nspr libvpx dav1d
	harfbuzz graphite pango aom libwebp libxss)

makedepends=(cbindgen clang diffutils imake inetutils lld llvm
             mesa nasm nodejs python rust unzip
             wasi-compiler-rt wasi-libc wasi-libc++ wasi-libc++abi
             xorg-server-xvfb yasm zip)

optdepends=('networkmanager: Location detection via available WiFi networks'
            'libnotify: Notification integration'
            'speech-dispatcher: Text-to-Speech'
            'hunspell-en_US: Spell checking, American English'
            'xdg-desktop-portal: Screensharing with Wayland')

options=(!emptydirs !makeflags !lto)
source=(https://archive.mozilla.org/pub/firefox/releases/$pkgver/source/firefox-$pkgver.source.tar.xz{,.asc}
	#0002-bmo-847568-Support-system-harfbuzz.patch
	#0003-bmo-847568-Support-system-graphite2.patch
	0012-bgo-816975-fix-build-on-x86.patch
	0018-build-Disable-Werror.patch
	0020-disable-watchdog.patch
	0022-bmo-1559213-Support-system-av1.patch
	0023-bmo-1874059-fix-libcxx-18.patch
	0027-bgo-925101-force-software-rendering-during-pgo-build.patch
	0029-bmo-1862601-system-icu-74.patch
	0030-enable-vaapi-on-all-amd-cards.patch
	1001-disable-healthreport-and-normandy.patch
	1002-disable-uitour.patch
	1003-new-logo-for-new-about-screen.patch
	vendor.js
	mozconfig
	identity-icons-brand.svg
	about-logo@2x.png
	$pkgname.desktop)
sha256sums=('201fcb12c285470f2b3cdf050f0fe7d96d5e0c5d352e86ef8957b595329a32bf'
            'SKIP'
            '1d4add2a4afff50e4067ef4c404f781513c1c388126754fba1bf0323b1e093b3'
            'e773d271fd7c74a725fb2709d6e4132dcfcf4df5a90c8c27d89a0d58330f7968'
            '29f7e2717c0e77f1e98cfc0b4fd99dfe5cc49222f8dc75fb91089a1b75e8d530'
            '00149f418212d609be22f2ddd30e97997c12faeebd10e5f66eed88a12f7b9536'
            '339c9d5bb3fd4de138e18cfa65d4137b705b32230a77b62e8a70612b531f700e'
            '61cca20aaaf732a9899978eb88b4d7f2621e6fbe6b4f2cc9d7b2af59ab25149b'
            'b07223e5928a5a0d4cb53e5c1a80cd93289f2f69a622c08e76d41a2434277ecc'
            '4b9d46a5234b1a85860bb25498e64b3835a87eb66310b1dba453cd3d5b19d9f1'
            '08444203b20bcfe5d97541aaefcd371d990216ab6193a50cb5c70bf1d188b2d9'
            'b0d09ea422c0a833791e3a0416c0bf15241e4c3aef6b6e898f08b83521757adb'
            '84a0f508a4fb45ad33a4c489b669274659bfe1b29e3c38a372d4b52c31b5d2de'
            '65023d88e3402fc15bfc7f4fcc81486a1f076e3e9d5d8adf1c109f65304aac5e'
            '865dffe86ffe1a0cd13f3dee73551f5819506b41c0bc6ee865f2543911b2c08b'
            '3108c353eeb1d46e97ff16434694a25957b5d371a57925b39e3187db20f67c9e'
            '6f791b85debe8c12d542b2a9f1b6851aea7df28a2f52e762e09b5db8ec11a349'
            '1f241fdc619f92a914c75aece7c7c717401d7467c9a306458e106b05f34e5044')
validpgpkeys=('14F26682D0916CDD81E37B6D61B7B526D98F0353') # Mozilla Software Releases <release@mozilla.com>

# Google API keys (see http://www.chromium.org/developers/how-tos/api-keys)
# Note: These are for Arch Linux use ONLY. For your own distribution, please
# get your own set of keys. Feel free to contact foutrelis@archlinux.org for
# more information.
#_google_api_key=AIzaSyDwr302FpOSkGRpLlUpPThNTDPbXcIn_FM

# Mozilla API keys (see https://location.services.mozilla.com/api)
# Note: These are for Arch Linux use ONLY. For your own distribution, please
# get your own set of keys. Feel free to contact heftig@archlinux.org for
# more information.
#_mozilla_api_key=e05d56db0a694edc8b5aaebda3f2db6a

prepare() {
  mkdir mozbuild
  cd firefox-${pkgver/esr/}
  cat ../mozconfig > .mozconfig

  #echo "---- Arch"

  echo "---- Plasmafox"
  patch -Np1 -i ../1001-disable-healthreport-and-normandy.patch
  patch -Np1 -i ../1002-disable-uitour.patch
  patch -Np1 -i ../1003-new-logo-for-new-about-screen.patch

  echo "---- System libs"
  #patch -Np1 -i ../0002-bmo-847568-Support-system-harfbuzz.patch
  #patch -Np1 -i ../0003-bmo-847568-Support-system-graphite2.patch
  patch -Np1 -i ../0022-bmo-1559213-Support-system-av1.patch

  echo "---- Other gentoo patches"
  patch -Np1 -i ../0012-bgo-816975-fix-build-on-x86.patch
  patch -Np1 -i ../0018-build-Disable-Werror.patch
  patch -Np1 -i ../0020-disable-watchdog.patch
  patch -Np1 -i ../0023-bmo-1874059-fix-libcxx-18.patch
  patch -Np1 -i ../0027-bgo-925101-force-software-rendering-during-pgo-build.patch
  patch -Np1 -i ../0029-bmo-1862601-system-icu-74.patch
  patch -Np1 -i ../0030-enable-vaapi-on-all-amd-cards.patch

  cp ../about-logo@2x.png ./browser/branding/official/content/

  # Make LTO respect MAKEOPTS (gentoo)
  sed -i -e "s/multiprocessing.cpu_count()/$(nproc)/" ./build/moz.configure/lto-pgo.configure
  sed -i -e "s/multiprocessing.cpu_count()/$(nproc)/" ./third_party/python/gyp/pylib/gyp/input.py

  #echo -n "$_google_api_key" >google-api-key
  #echo -n "$_mozilla_api_key" >mozilla-api-key
}

build() {
  cd firefox-${pkgver/esr/}

  export LC_ALL=C
  export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=pip
  export MOZBUILD_STATE_PATH="$srcdir/mozbuild"
  export MOZ_BUILD_DATE="$(date -u${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH} +%Y%m%d%H%M%S)"
  export MOZ_NOSPAM=1
  export RUSTFLAGS+=" -C debuginfo=1"
  #export DISPLAY=":99"
  export LLVM_PROFDATA=llvm-profdata

  # malloc_usable_size is used in various parts of the codebase
  CFLAGS="${CFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"
  CXXFLAGS="${CXXFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"
  # Breaks compilation since https://bugzilla.mozilla.org/show_bug.cgi?id=1896066
  CFLAGS="${CFLAGS/-fexceptions/}"
  CXXFLAGS="${CXXFLAGS/-fexceptions/}"
  #
  LDFLAGS+=" -Wl,--undefined-version -Wl,--compress-debug-sections=zlib" # -flto=thin"

  ulimit -n 16384
  # Avoid PGO profiling problems due to enviroment leakage
  # These should *always* be cleaned up anyway
  unset \
	DBUS_SESSION_BUS_ADDRESS \
	ORBIT_SOCKETDIR \
	SESSION_MANAGER \
	XAUTHORITY \
	XDG_CACHE_HOME \
	XDG_SESSION_COOKIE

  # Do 3-tier PGO
  echo "Building instrumented browser..."
  cat >.mozconfig ../mozconfig - <<END
ac_add_options --enable-profile-generate=cross
END
  ./mach build --priority normal

  echo "Profiling instrumented browser..."
  ./mach package
  JARLOG_FILE="$PWD/jarlog" \
     xvfb-run -s "-screen 0 1920x1080x24 -shmem -nolisten local" \
    ./mach python build/pgo/profileserver.py

  stat -c "Profile data found (%s bytes)" merged.profdata
  test -s merged.profdata

  stat -c "Jar log found (%s bytes)" jarlog
  test -s jarlog

  echo "Removing instrumented browser..."
  ./mach clobber objdir

  echo "Building optimized browser..."
  cat >.mozconfig ../mozconfig - <<END
ac_add_options --enable-lto=cross,thin
ac_add_options --enable-profile-use=cross
ac_add_options --with-pgo-profile-path=${PWD@Q}/merged.profdata
ac_add_options --with-pgo-jarlog=${PWD@Q}/jarlog
END
  ./mach build --priority normal
}

package() {
  cd firefox-${pkgver/esr/}
  DESTDIR="$pkgdir" ./mach install

  install -Dvm644 ../vendor.js "$pkgdir/usr/lib/firefox/browser/defaults/preferences/vendor.js"

  local distini="$pkgdir/usr/lib/$pkgname/distribution/distribution.ini"
  install -Dvm644 /dev/stdin "$distini" <<END
[Global]
id=archlinux
version=1.0tv
about=Mozilla Firefox for Arch Linux

[Preferences]
app.distributor=archlinux
app.distributor.channel=$pkgname
app.partner.archlinux=archlinux
END

  local i
  for i in 16 22 24 32 48 64 128 256; do
    install -Dvm644 browser/branding/official/default$i.png \
      "$pkgdir/usr/share/icons/hicolor/${i}x${i}/apps/$pkgname.png"
  done
  install -Dvm644 browser/branding/official/content/about-logo.png \
    "$pkgdir/usr/share/icons/hicolor/192x192/apps/$pkgname.png"
  install -Dvm644 browser/branding/official/content/about-logo@2x.png \
    "$pkgdir/usr/share/icons/hicolor/384x384/apps/$pkgname.png"
  install -Dvm644 browser/branding/official/content/about-logo.svg \
    "$pkgdir/usr/share/icons/hicolor/scalable/apps/$pkgname.svg"
  install -Dvm644 ../identity-icons-brand.svg \
    "$pkgdir/usr/share/icons/hicolor/symbolic/apps/$pkgname-symbolic.svg"

  install -Dvm644 ../$pkgname.desktop \
    "$pkgdir/usr/share/applications/$pkgname.desktop"

  # Install a wrapper to avoid confusion about binary path
  install -Dvm755 /dev/stdin "$pkgdir/usr/bin/$pkgname" <<END
#!/bin/sh
exec /usr/lib/$pkgname/firefox "\$@"
END

  # Replace duplicate binary with wrapper
  # https://bugzilla.mozilla.org/show_bug.cgi?id=658850
  ln -srfv "$pkgdir/usr/bin/$pkgname" "$pkgdir/usr/lib/$pkgname/firefox-bin"

  # Use system certificates
  local nssckbi="$pkgdir/usr/lib/$pkgname/libnssckbi.so"
  if [[ -e $nssckbi ]]; then
    ln -srfv "$pkgdir/usr/lib/libnssckbi.so" "$nssckbi"
  fi
}
